#!/bin/bash

kubectl create -f wekan-db-service.yaml
kubectl create -f wekan-app-service.yaml

kubectl create -f wekan-app-deployment.yaml
kubectl create -f wekan-db-deployment.yaml
